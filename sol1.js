const binominalCoefficientMod3 = (a, b) => {
  let result = 1;
  let temp = 0;
  let a3 = 0, b3 = 0;
  while( a > 0){
    a3 = +(a%3).toString();
    b3 = +(b%3).toString();
    if(b3 > a3){
      return 0;
    };
    temp = (b3 == 0 || b3 == a3)? 1: 2;
    result = +(((result * temp) % 3).toString())
    a = +(((a - a%3)/3).toString());
    b = +(((b - b%3)/3).toString());
  }
  return result;
}

const colorToNumber = (color) => {
  if(color == 'R'){
    return 0
  }
  if(color == 'G'){
    return 1
  }
  return 2
}

const numberToColor = (number) => {
  if(number == 0){
    return 'R'
  }
  if(number == 1){
    return 'G'
  }
  return 'B'
}

const triangle = (row) => {
  let n = row.length - 1;
  let temp = 0;
  let result = 0;
  for(let i = 0; i<row.length; i++){
    temp = binominalCoefficientMod3(n, i) * colorToNumber(row[i]);
    result = +(((result + temp)%3).toString());
  }

  if(n%2 == 1){
    result = (-result)%3 == 0?(-result)%3:(-result)%3+3;
  }

  return numberToColor(result);
}